# node-crud

Node.js and npm: Ensure that Node.js and npm are installed on your machine. You can download them from here.

Docker: You will need Docker to build and run your application in a containerized environment. Download and install Docker from here.

GitLab Account: Ensure you have a GitLab account to set up CI/CD.

Digital Ocean Account: Ensure you have a Digital Ocean account and have set up a droplet


### Setting Up Development Environment

Clone the repository

```
git clone https://github.com/ferrridd/node-crud.git
cd your-repo
```
Install packages
```
npm install
```

Run node application
```
npm start
```

### Build docker image
```
docker build -t node-crud .
```

Run docker image
```
docker run -p 3000:3000 -d my-express-app
```

### Deploying Using GitLab CI/CD

I have configured a GitLab CI/CD pipeline with three stages: build, test, and deploy.
Pipeline Stages:

    Build: In this stage, the Docker image of the application is built.
    Test: Here, any tests for the application are run to ensure functionality.
    Deploy: In the final stage, the application's files are copied to a Digital Ocean droplet, built there, and exposed from port 3000 to port 80.

### Accessing the Deployed Application:

```
[ferrridd.com](http://159.65.192.207/)
```
